"""
MIT License

Copyright (c) 2020-present The-Naomi-Developers

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from discord import Interaction
from discord.ui import Button, View
from .properties import ButtonProperties


class PreviousPageButton(Button):
    def __init__(self, view: View, props: ButtonProperties):
        self.current_view = view
        super().__init__(style=props.style, label=props.label, emoji=props.emoji)

    async def callback(self, i: Interaction) -> None:
        await self.current_view._move(i, -1)


class NextPageButton(Button):
    def __init__(self, view: View, props: ButtonProperties):
        self.current_view = view
        super().__init__(style=props.style, label=props.label, emoji=props.emoji)

    async def callback(self, i: Interaction) -> None:
        await self.current_view._move(i, 1)


class CloseButton(Button):
    def __init__(self, view: View, props: ButtonProperties):
        self.current_view = view
        super().__init__(style=props.style, label=props.label, emoji=props.emoji)

    async def callback(self, i: Interaction) -> None:
        await self.current_view._close()
